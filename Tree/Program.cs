﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Tree
{
    class Program
    {
        static void Main(string[] args)
        {
            BST<int> tree = null;

            BST<int>.Insert(ref tree, 16);
            BST<int>.Insert(ref tree, 9);
            BST<int>.Insert(ref tree, 85);
            BST<int>.Insert(ref tree, 86);
            BST<int>.Insert(ref tree, 0);
            BST<int>.Insert(ref tree, 485);
            BST<int>.Insert(ref tree, 62);

            BST<int>.PostOrderPrint(tree);
            Console.WriteLine();

            BST<int>.LevelOrderPrint(tree);
            Console.WriteLine();
            
            BST<int>.BalanceTree(ref tree);
            BST<int>.PostOrderPrint(tree);
            Console.WriteLine();

            Console.WriteLine("Height is {0}",BST<int>.Height(tree));
        }
    }
}
