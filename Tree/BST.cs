﻿﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Tree
{
    class BST<T> where T : IComparable
    {
        public BST<T> Left;
        public BST<T> Right;
        T _data;

        public BST(T data)
        {
            _data = data;
        }
        public static void PostOrderPrint(BST<T> root)
        {   
            if(root != null)
            {
                PostOrderPrint(root.Left);
                PostOrderPrint(root.Right);
                Console.WriteLine(root._data);
            }
        }   
        public static void Insert(ref BST<T> root, T data)
        {
            if (root == null) { root = new BST<T>(data); }
            else if (root._data.CompareTo(data) > 0) Insert(ref root.Left, data);
            else Insert(ref root.Right, data);
        }
        public static void LevelOrderPrint(BST<T> root)
        {
            if (root == null)
                throw new Exception("Tree is empty");
            Queue<BST<T>> torrent = new Queue<BST<T>>();
            torrent.Enqueue(root);

            while (torrent.Count != 0)
            {
                BST<T> temp = torrent.Dequeue();
                if (temp.Left != null) torrent.Enqueue(temp.Left);
                if (temp.Right != null) torrent.Enqueue(temp.Right);
                Console.WriteLine(temp._data);
            }
        }        
        public static int Height(BST<T> root)
        {
            int height = -1;
            if (root == null)
                return height;
            int Lheight = Height(root.Left);
            int Rheight = Height(root.Right);
            return Math.Max(Lheight, Rheight) + 1;
        }
        private static void GenerateList(BST<T> root, ref List<T> list)
        {
            if (root == null)
                return;
            GenerateList(root.Left, ref list);
            list.Add(root._data);
            GenerateList(root.Right, ref list);
        }
        public static void BalanceTree(ref BST<T> root)
        {
            List<T> ListOfData = new List<T>();
            GenerateList(root, ref ListOfData);
            root = null;
            ListOfData.Sort();
            Insert(ref root, ListOfData[ListOfData.Count / 2]);
            foreach(var data in ListOfData)
            {
                if (ListOfData.IndexOf(data) == ListOfData.Count / 2) 
                    continue;
                Insert(ref root, data);
            }
        }        
    }
}